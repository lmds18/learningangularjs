﻿(function () {
    var appStore = angular.module('store-directives', []);

    appStore.directive("productDescription", function () {
        return {
            restrict: 'E',
            templateUrl: "productViews/product-description.html"
        };
    });

    appStore.directive("productReviews", function () {
        return {
            restrict: 'E',
            templateUrl: "productViews/product-reviews.html"
        };
    });

    appStore.directive("productSpecs", function () {
        return {
            restrict: "A",
            templateUrl: "productViews/product-specs.html"
        };
    });

    appStore.directive("productTabs", function () {
        return {
            restrict: "E",
            templateUrl: "productViews/product-tabs.html",
            controller: function () {
                this.tab = 1;

                this.isSet = function (checkTab) {
                    return this.tab === checkTab;
                };

                this.setTab = function (activeTab) {
                    this.tab = activeTab;
                };
            },
            controllerAs: "tab"
        };
    });

    appStore.directive("productGallery", function () {
        return {
            restrict: "E",
            templateUrl: "productViews/product-gallery.html",
            controller: function () {
                this.current = 0;
                this.setCurrent = function (imageNumber) {
                    this.current = imageNumber || 0;
                };
            },
            controllerAs: "gallery"
        };
    });

})();